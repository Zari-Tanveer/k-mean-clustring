# Final Product Demonstration
To develop a system that performs analysis on the dataset provided to figure out the geographic distribution of cases and discover clusters of high and low incidence. The system will be able to provide reports according to no of clusters (value of k).
- The system generates a report which contains statistics i.e. count, mean, max, std, centroids, etc. on the graphical distribution of data.
- The system provides graphical representation according to the dataset and clusters.
- To develop this system, K-mean clustering is used which comes under unsupervised learning as data is unlabelled developed in python where 
  centroids are changed when the value of k is changed and a new graph displayed according to the new value.

Below is a brief description of the system along with screenshots:
	
- **Dataset:**
<br>Below is the dataset that we used to perform the analysis:


<details><summary>Click to expand</summary>

</details>
